export class Person {
  private firstname: string;
  private middlename: string;
  private lastname: string;
  private age: number;

  constructor(
    firstname: string,
    middlename: string,
    lastname: string,
    age: number
  ) {
    this.firstname = firstname;
    this.middlename = middlename;
    this.lastname = lastname;
    this.age = age;
  }

  getFullName(): string {
    return this.firstname.concat(' ', this.middlename, ' ', this.lastname);
  }

  getBirthdayYear(): number {
    let year = new Date();
    let birthYear: number = year.getFullYear() - this.age;
    return birthYear;
  }
}
