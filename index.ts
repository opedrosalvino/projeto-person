import './style.css';
import { Person } from './person';

let person = new Person('Pedro Luiz', 'Souza', 'Salvino', 21);
const fullname: HTMLElement = document.getElementById('person.name');
const birthyear: HTMLElement = document.getElementById('person.birthyear');

fullname.innerHTML = person.getFullName();
birthyear.innerHTML = `Nascido no ano ${person.getBirthdayYear()}`;
